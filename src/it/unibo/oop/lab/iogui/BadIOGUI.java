package it.unibo.oop.lab.iogui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * This class is a simple application that writes a random number on a file.
 * 
 * This application does not exploit the model-view-controller pattern, and as
 * such is just to be used to learn the basics, not as a template for your
 * applications.
 */
public class BadIOGUI {

    private static final String TITLE = "A very simple GUI application";
    private static final String PATH = System.getProperty("user.home")
            + System.getProperty("file.separator")
            + BadIOGUI.class.getSimpleName() + ".txt";//utilizzo system.getpropiety per saper le proprietà dell'ambient ein cui mi trova questo fa si che questo codice possa essere eseguito dappertutto
    private static final int PROPORTION = 5;
    private final Random rng = new Random();
    private final JFrame frame = new JFrame(TITLE);

    /**
     * 
     */
    public BadIOGUI() {//costruttore di badgui
        final JPanel canvas = new JPanel();//creo pannelo canvas
        final JPanel myPanel = new JPanel();//creo pannello panel
        myPanel.setLayout(new BoxLayout(myPanel,BoxLayout.X_AXIS ));//imposto layout a boxlayout
        canvas.setLayout(new BorderLayout());//layout di canvas a border layout
        canvas.add(myPanel, BorderLayout.CENTER);//inserisco il mio pannello in canvas
        final JButton write = new JButton("Write");//creo nuovo pulsante write
        final JButton read = new JButton("Read");//creo nuovo pulsante read
        myPanel.add(write);//aggiungo write al pannello
        myPanel.add(read);//aggiungo read al pannello
        frame.setContentPane(canvas);//metto come contenuto del pannello canvas
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*
         * Handlers
         */
        write.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                /*
                 * This would be VERY BAD in a real application.
                 * 
                 * This makes the Event Dispatch Thread (EDT) work on an I/O
                 * operation. I/O operations may take a long time, during which
                 * your UI becomes completely unresponsive.
                 */
                try (PrintStream ps = new PrintStream(PATH)) {
                    Integer num = rng.nextInt();
                    ps.print(num);
                    System.out.println(num);
                } catch (FileNotFoundException e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
            }
        });
        read.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                try {
                    final List<String> lines = Files.readAllLines(new File(PATH).toPath());
                    for (final String line: lines) {
                        System.out.println(line);
                    }
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
                /*
                 * try{
                 *  String line = line;
                 *  BufferedReader in = new BufferedReader(new FileReader(PATH));
                 *  line = in.readLine();
                 *  while(in!=null){
                 *      system.out.println(line);
                 *      line = in.readLine;
                 *  }
                 * }
                 */
            }
        });
    }

    private void display() {
        /*
         * Make the frame one fifth the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected. In order to deal coherently with multimonitor
         * setups, other facilities exist (see the Java documentation about this
         * issue). It is MUCH better than manually specify the size of a window
         * in pixel: it takes into account the current resolution.
         */
        //final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();//toolkit consente di averev la risoluzione attuale dello schermo e di conseguenza ridimensiona la finestra correttamente
        //final int sw = (int) screen.getWidth();
        //final int sh = (int) screen.getHeight();
        //frame.setSize(sw / PROPORTION, sh / PROPORTION);
        frame.pack();
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);//delega il posizionamento della finestra al sistema operativo
        /*
         * OK, ready to pull the frame onscreen
         */
        frame.setVisible(true);
    }

    /**
     * @param args ignored
     */
    public static void main(final String... args) {
       new BadIOGUI().display();
    }
}
