package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ControllerImp implements Controller {

    List<String> strings;
    String current;
    
    public ControllerImp() {
        this.strings  = new ArrayList<>();
    }

    @Override
    public void setString(String str) {
        Objects.requireNonNull(str);
        strings.add(str);

    }

    @Override
    public String next() {
       this.current = this.strings.iterator().next();
       return this.current;
    }

    @Override
    public List<String> historyOfStrings() {
        return this.strings;
    }

    @Override
    public void currentString() {
        if(this.strings.iterator().hasNext()) {
            System.out.println(this.current);
        }else {
            throw new IllegalStateException() ;
        }

    }

}
