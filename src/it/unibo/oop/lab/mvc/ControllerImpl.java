package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;

public class ControllerImpl implements Controller {

    List<String> strings;
    String current;
    
    public ControllerImpl() {
        this.strings = new LinkedList<>();
    }

  
    public void setString(String str) {
        this.strings.add(str);

    }

    public void hasNext() {
       if(this.strings.iterator().hasNext()) {
           this.current = this.strings.iterator().next();
       }

    }

    @Override
    public List<String> historyOfStrings() {
        return this.strings;
    }

    @Override
    public void currentString() {
       System.out.println(this.current);

    }

}
