package it.unibo.oop.lab.mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {
    private static final String TITLE = "A very simple GUI application";
    private static final int PROPORTION = 5;
    private final JFrame frame = new JFrame(TITLE);
    
    public static void main() {
        
    }

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextField in the upper part of the frame, 
     * a JTextArea in the center and two buttons below it: "Print", and "Show history". 
     * SUGGESTION: Use a JPanel with BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The behavior of the program is that, if "Print" is pressed, the
     * controller is asked to show the string contained in the text field on standard output. 
     * If "show history" is pressed instead, the GUI must show all the prints that
     * have been done to this moment in the text area.
     * 
     */
   

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI(ControllerImp ctrl) {

        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final JPanel canvas = new JPanel();
        canvas.setLayout(new BorderLayout());
        final JTextField textFieald = new JTextField();
        final JTextArea textArea = new JTextArea();
        final JPanel panel2 = new JPanel(new FlowLayout());
        final JButton printButton = new JButton("Print");
        final JButton historyButton = new JButton("Show history");
        canvas.add(textFieald, BorderLayout.NORTH);
        canvas.add(textArea, BorderLayout.CENTER);
        panel2.add(printButton);
        panel2.add(historyButton);
        canvas.add(panel2, BorderLayout.SOUTH);
        frame.setContentPane(canvas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        printButton.addActionListener(e-> {
            ctrl.setString(textFieald.getText());
            System.out.println(textFieald.getText());
            });
        historyButton.addActionListener(e->{
            List<String> history = ctrl.historyOfStrings();
            for(String elem : history) {
                textArea.insert(" "+elem, 0);;
            }
        });
        
        
        
        
        
        
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);

        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
    }
    
    public void display() {
        frame.setVisible(true);
    }
    
    public static void main(final String... args) {
        ControllerImp ctrl = new ControllerImp();
        new SimpleGUI(ctrl).display();
    }

}
